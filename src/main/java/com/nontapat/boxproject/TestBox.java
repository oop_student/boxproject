/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.boxproject;

/**
 *
 * @author DELL
 */
public class TestBox {
    public static void main(String[] args) {
        Box box1 = new Box(7,10,17,"Black");
        System.out.println("box1("+box1.getWidth()+","+box1.getLength()+","+
                box1.getHeight()+") volume: "+box1.calVol()+" cm^3");
        System.out.println("box1("+box1.getWidth()+","+box1.getLength()+","+
                box1.getHeight()+") area: "+box1.calArea()+" cm^2");
        System.out.println("box1 color: "+box1.getColor());
        box1.setLength(0);
        box1.setLength(4);
        box1.setWidth(2.5);
        box1.setHeight(8);
        box1.setColor("Black&Yellow");
        System.out.println("box1("+box1.getWidth()+","+box1.getLength()+","+
                box1.getHeight()+") volume: "+box1.calVol()+" cm^3");
        System.out.println("box1("+box1.getWidth()+","+box1.getLength()+","+
                box1.getHeight()+") area: "+box1.calArea()+" cm^2");
        System.out.println("box1 color: "+box1.getColor());
    }
}

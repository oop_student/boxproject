/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.boxproject;

/**
 *
 * @author DELL
 */
public class Box {
    private double width;
    private double length;
    private double height;
    private String color;

    public Box(double width, double length, double height, String color) {
        this.width = width;
        this.length = length;
        this.height = height;
        this.color = color;
    }
    
    public double calArea(){
        return 2*(width*length)+2*(length*height)+2*(width*height);
    }
    
    public double calVol(){
        return width*length*height;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public String getColor() {
        return color;
    }

    public void setWidth(double width) {
        if (width<= 0) {
            System.out.println("Error: Width must more than zero!!!!");
            return;
        }
        this.width = width;
    }

    public void setLength(double length) {
         if (length<= 0) {
            System.out.println("Error: Length must more than zero!!!!");
            return;
        }
        this.length = length;
    }

    public void setHeight(double height) {
         if (height<= 0) {
            System.out.println("Error: Height must more than zero!!!!");
            return;
        }
        this.height = height;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
